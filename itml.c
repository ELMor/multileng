/*
	El formato de itml.txt debe ser el siguiente:

            ---------T1-------
	{NUMITEMS|ITEMNUMBER|POS|LEN|}{MSG1MSG2...MSGN}
                       ---T2--
	NUMITEMS: numero de items totales en el archivo
	ITEMNUMBER: el primary key de la tabla items_multileng (campo 'item')
	POS: La posici�n dentro del archivo itml.txt
	LEN: longitud del mensaje.

	El conjunto T1 se repite NUMITEMS veces
	El conjunto T2 se repite NUMLAN veces (n�mero de idiomas, 10 en nuestro caso)

	Finalmente se concatenan los mensajes MSG1MSG2 al final del archivo.

	Un ejemplo de c�mo generar itml.txt partiendo de la BBDD se encuentra en
	el programa Java Main.java en este mismo directorio.

*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ITEMSFILE "itml.txt"
#define LENNUM		10
#define NUMLAN		10
#undef DEBUG

typedef struct _item {
	long item;
	long pos[10];
	long len[10];
} _item;

static _item *items;
static long numitems;
static long headoff;
static int initialized=0;
static FILE* itfile;
static char message[1024];

int Init(void);
int compItem(const void *p1, const void *p2);

int Init(){
	int i,j;
	char s[LENNUM+1];

	#ifdef DEBUG
		printf("Inicializando\n");
	#endif

	if(NULL==(itfile=fopen(ITEMSFILE,"rt")))
		return -1;

	#ifdef DEBUG
		printf("Fichero %s Abierto\n",ITEMSFILE);
	#endif


	fread(s,1,LENNUM,itfile);
	s[LENNUM]=0;
	numitems=atoi( s );
	items=(_item*)malloc(numitems*sizeof(_item));

	#ifdef DEBUG
		printf("Numero de items:%d\n",numitems);
	#endif

	//Leemos los items
	for(i=0;i<numitems;i++){
		fread(s,1,LENNUM,itfile);
		s[LENNUM]=0;
		items[i].item=atoi(s);

		for(j=0;j<NUMLAN;j++){
			fread(s,1,LENNUM,itfile);
			s[LENNUM]=0;
			items[i].pos[j]=atoi(s);

			fread(s,1,LENNUM,itfile);
			s[LENNUM]=0;
			items[i].len[j]=atoi(s);
		}
	}
	headoff=ftell(itfile);
	initialized=1;
	return 0;
}


int compItem(const void *p1, const void *p2){
	long *l1,*l2;

	l1=(long*)p1;
	l2=(long*)p2;
	return *l1-*l2;
}

// Funcion de busqueda

long pascal _export get_item_ml(long itn,long lan, char *msg){
	_item *pos;
	char s[1+LENNUM];
	FILE *fp;

	if(!initialized)
		if( Init() )
			return 0;

	pos=bsearch(&itn,items,numitems,sizeof(_item),compItem);
	if(!pos){
		*msg=0;
		return 0;
	}
	//El item est� en la posici�n 'foundPos'
	fseek(itfile,headoff-1+pos->pos[lan-1],SEEK_SET);
	fread(message,1,pos->len[lan-1],itfile);
	message[pos->len[lan-1]]=0;
	strcpy(msg,message);
	return 1;
}


int main(int argc, char *argv[]){
	char s[256];
	long iml,lan,ikj;
	long ret;

	if(argc!=3){
		puts("Uso: itml item lang");
		return 0;
	}

	iml=atoi(argv[1]);
	lan=atoi(argv[2]);

	printf("item# %d, Lang # %d\n",iml,lan);
	fflush(stdout);

	for(ikj=0;ikj<5;ikj++){
		ret=get_item_ml(iml+ikj,lan,s);
		printf("Return %d:",ret);
		printf("Item %d:%s%s\n",iml+ikj,s==NULL ? "Not found" :"Item=",s==NULL ? "" : s);
	}
	return 0;
}
