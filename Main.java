/*@lineinfo:filename=Main*//*@lineinfo:user-code*//*@lineinfo:1^1*/
import java.text.*;
import oracle.sqlj.runtime.Oracle;

/*@lineinfo:generated-code*//*@lineinfo:5^1*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

class im 
extends sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public im(sqlj.runtime.profile.RTResultSet resultSet) 
    throws java.sql.SQLException 
  {
    super(resultSet);
    itemNdx = findColumn("item");
    lg1Ndx = findColumn("lg1");
    lg2Ndx = findColumn("lg2");
    lg3Ndx = findColumn("lg3");
    lg4Ndx = findColumn("lg4");
    lg5Ndx = findColumn("lg5");
    lg6Ndx = findColumn("lg6");
    lg7Ndx = findColumn("lg7");
    lg8Ndx = findColumn("lg8");
    lg9Ndx = findColumn("lg9");
    lg10Ndx = findColumn("lg10");
  }
  public int item() 
    throws java.sql.SQLException 
  {
    return resultSet.getIntNoNull(itemNdx);
  }
  private int itemNdx;
  public String lg1() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg1Ndx);
  }
  private int lg1Ndx;
  public String lg2() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg2Ndx);
  }
  private int lg2Ndx;
  public String lg3() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg3Ndx);
  }
  private int lg3Ndx;
  public String lg4() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg4Ndx);
  }
  private int lg4Ndx;
  public String lg5() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg5Ndx);
  }
  private int lg5Ndx;
  public String lg6() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg6Ndx);
  }
  private int lg6Ndx;
  public String lg7() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg7Ndx);
  }
  private int lg7Ndx;
  public String lg8() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg8Ndx);
  }
  private int lg8Ndx;
  public String lg9() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg9Ndx);
  }
  private int lg9Ndx;
  public String lg10() 
    throws java.sql.SQLException 
  {
    return resultSet.getString(lg10Ndx);
  }
  private int lg10Ndx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:17^1*/

class Main {
	private DecimalFormat nf1;

	int msg2p( int pos, String k ){
		if( k==null ){
			System.out.print(nf1.format(pos));
			System.out.print(nf1.format(0));
			return pos;
		}else{
			System.out.print(nf1.format(pos));
			System.out.print(nf1.format(k.length()));
			System.err.print(k);
			return pos+k.length();
		}
	}

	public static void main( String argv[] ){
		new Main(argv);
	}

	public Main(String argv[]){
		int numItems;
		int pos;
		nf1=new DecimalFormat();
		nf1.applyPattern("0000000000");
		try {
			Oracle.connect(
				"jdbc:oracle:thin:@"+argv[0]+":"+argv[1]+":"+argv[2],
				argv[3],
				argv[4]);
			/*@lineinfo:generated-code*//*@lineinfo:49^4*/

//  ************************************************************
//  #sql { Select count(*)  from items_multileng
//  			 };
//  ************************************************************

{
  sqlj.runtime.profile.RTResultSet __sJT_rtRs;
  sqlj.runtime.ConnectionContext __sJT_connCtx = sqlj.runtime.ref.DefaultContext.getDefaultContext();
  if (__sJT_connCtx == null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext __sJT_execCtx = __sJT_connCtx.getExecutionContext();
  if (__sJT_execCtx == null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_EXEC_CTX();
  synchronized (__sJT_execCtx) {
    sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_connCtx, Main_SJProfileKeys.getKey(0), 0);
    try 
    {
      sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
      __sJT_rtRs = __sJT_result;
    }
    finally 
    {
      __sJT_execCtx.releaseStatement();
    }
  }
  try 
  {
    sqlj.runtime.ref.ResultSetIterImpl.checkColumns(__sJT_rtRs, 1);
    if (!__sJT_rtRs.next())
    {
      sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
    }
    numItems = __sJT_rtRs.getIntNoNull(1);
    if (__sJT_rtRs.next())
    {
      sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
    }
  }
  finally 
  {
    __sJT_rtRs.close();
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:51^4*/
			System.out.print(nf1.format(numItems));
			pos=10+numItems*210;
			im items;
			/*@lineinfo:generated-code*//*@lineinfo:55^4*/

//  ************************************************************
//  #sql items = { Select item,lg1,lg2,lg3,lg4,lg5,lg6,lg7,lg8,lg9,lg10
//  				From   items_multileng
//  				Order  by item
//  			 };
//  ************************************************************

{
  sqlj.runtime.ConnectionContext __sJT_connCtx = sqlj.runtime.ref.DefaultContext.getDefaultContext();
  if (__sJT_connCtx == null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext __sJT_execCtx = __sJT_connCtx.getExecutionContext();
  if (__sJT_execCtx == null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_EXEC_CTX();
  synchronized (__sJT_execCtx) {
    sqlj.runtime.profile.RTStatement __sJT_stmt = __sJT_execCtx.registerStatement(__sJT_connCtx, Main_SJProfileKeys.getKey(0), 1);
    try 
    {
      sqlj.runtime.profile.RTResultSet __sJT_result = __sJT_execCtx.executeQuery();
      items = new im(__sJT_result);
    }
    finally 
    {
      __sJT_execCtx.releaseStatement();
    }
  }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:59^4*/
			while( items.next() ){
				System.out.print(nf1.format(items.item()));
				pos=msg2p( pos,items.lg1() );
				pos=msg2p( pos,items.lg2() );
				pos=msg2p( pos,items.lg3() );
				pos=msg2p( pos,items.lg4() );
				pos=msg2p( pos,items.lg5() );
				pos=msg2p( pos,items.lg6() );
				pos=msg2p( pos,items.lg7() );
				pos=msg2p( pos,items.lg8() );
				pos=msg2p( pos,items.lg9() );
				pos=msg2p( pos,items.lg10() );
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}/*@lineinfo:generated-code*/class Main_SJProfileKeys 
{
  private static Main_SJProfileKeys inst = null;
  public static java.lang.Object getKey(int keyNum) 
    throws java.sql.SQLException 
  {
    if (inst == null)
    {
      inst = new Main_SJProfileKeys();
    }
    return inst.keys[keyNum];
  }
  private final sqlj.runtime.profile.Loader loader = sqlj.runtime.RuntimeContext.getRuntime().getLoaderForClass(getClass());
  private java.lang.Object[] keys;
  private Main_SJProfileKeys() 
    throws java.sql.SQLException 
  {
    keys = new java.lang.Object[1];
    keys[0] = sqlj.runtime.ref.DefaultContext.getProfileKey(loader, "Main_SJProfile0");
  }
}
