
compile : Main.class itml.exe itml.dll

run : compile
	java Main 10.111.96.99 1521 lx01 cla cla > head 2> tail
	cat head tail > itml.txt

Main.class : Main.java Makefile
	javac Main.java

Main.java : Main.sqlj Makefile
	sqlj -compile=false Main.sqlj

itml.exe : itml.c Makefile
	bcc32 -a1 itml.c

itml.dll : itml.c Makefile
	bcc32 -a1 -tWD itml.c
