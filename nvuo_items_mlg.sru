$PBExportHeader$nvuo_items_mlg.sru
forward
global type nvuo_items_mlg from uonv_ngcomodin
end type
end forward

global type nvuo_items_mlg from uonv_ngcomodin
end type
global nvuo_items_mlg nvuo_items_mlg

type prototypes
public function long gim32(ref string m, long lan, long itn) library "itml.dll" alias for "GET_ITEM_ML"
end prototypes
type variables

Long  item

String is_mensaje



end variables

forward prototypes
public function long uof_init (long arg_item, long arg_idioma, any arg_parametros)
public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono, any arg_buttom, long arg_defecto)
public function Long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, any arg_paramb, long arg_idioma)
public function Long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono)
public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono, any arg_buttom)
public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, long arg_idioma)
public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, long arg_idioma, any arg_icono)
public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, long arg_idioma, any arg_icono, any arg_buttom)
public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, long arg_idioma, any arg_icono, any arg_buttom, long arg_defecto)
public function long uof_messagebox (long arg_itema, long arg_itemb, any arg_paramb, long arg_idioma)
public function long uof_messagebox (long arg_itema, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono)
public function long uof_messagebox (long arg_itema, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono, any arg_buttom)
public function long uof_messagebox (long arg_itema, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono, any arg_buttom, long arg_defecto)
public function long uof_messagebox (long arg_itema, long arg_itemb, long arg_idioma)
public function long uof_messagebox (long arg_itema, long arg_itemb, long arg_idioma, any arg_icono)
public function long uof_messagebox (long arg_itema, long arg_itemb, long arg_idioma, any arg_icono, any arg_buttom)
public function long uof_messagebox (long arg_itema, long arg_itemb, long arg_idioma, any arg_icono, any arg_buttom, long arg_defecto)
private function string uof_sustituir_cadena (string arg_cadena, string arg_buscada, string arg_sustituta)
public function string uof_init (long al_msg_pk, any aa_args)
public function string uof_init (long al_msg_pk)
public function long uof_messagebox (long al_msg_id)
public function long uof_formatear (ref datawindow adw)
public function long uof_formatear (ref userobject auo)
public function long uof_formatear (datastore ads)
public function long uof_formatear (datawindowchild adwc)
end prototypes

public function long uof_init (long arg_item, long arg_idioma, any arg_parametros);//
// INTERFAZ
// Descripci�n:	Obtiene de la tabla items_mlg el item correspondiente al lenguaje indicado,
//						y lo almacena en la variable de instancia "is_mensaje".
//						Sustituye a la funci�n global item_mlg_ext.
//
//			<uof_init>
//
// IN
//		Long 	arg_item				=> N� del item que se quiere obtener.
//		Long  arg_idioma			=> Idioma desado, vendr� dado por la variable global gl_idioma.
//		Any	arg_parametros		=> Deber�a ser siempre o bien " " o bien un array de cadenas.
// 
// OUT
//		Long 	ll_fila				=> Descripci�n del item indicado.
//
// Creado	<27/10/1.999>		<M��ngeles Gracia>			<R-NH-2534-274>
//__________________________________________________________________________________________


Long ll_fila
Long ll_contador
String ls_parametro
String lsa_parametros[]


//Eladio ll_fila = ids_nvuo.Retrieve (arg_item)
//Eladio
is_mensaje=space(1024)
if (arg_idioma>0) and (arg_idioma<11) then
	ll_fila=gim32( is_mensaje, arg_idioma,arg_item )
else
	ll_fila=gim32(is_mensaje,1,arg_item);
end if

If (ll_fila = 1) Then
	item = arg_item
	If (arg_idioma > 0) And (arg_idioma <= 10) Then
		//Eladio is_mensaje = ids_nvuo.GetItemString (ll_fila, 'lg' + String (arg_idioma))
		If IsNull (is_mensaje) Then
			is_mensaje = "El item '" + String(arg_item) + "' no est� definido para el lenguaje  " + 'lg' + String (arg_idioma)
			ll_fila = -1
		End If
	Else
		//Eladio is_mensaje = ids_nvuo.GetItemString (ll_fila, 'lg1')
		If IsNull (is_mensaje) Then
			is_mensaje = "El item '" + String(arg_item) + "' no est� definido para el lenguaje por DEFECTO"
			ll_fila = -1
		End If
	End If
	
Else
	is_mensaje = "El item '" + String(arg_item) +"' no existe en tabla de mensajes (items_multileng)"
End If

If (ll_fila > 0) Then
	is_mensaje = uof_sustituir_cadena (is_mensaje ,"~~n", "~n")
	is_mensaje = uof_sustituir_cadena (is_mensaje, "~~r", "~r")
	is_mensaje = uof_sustituir_cadena (is_mensaje, "~~t", "~t")
	
	If Not IsNull (arg_parametros) Then
	
		ls_parametro = String (arg_parametros)
	
		If Len(Trim(ls_parametro)) > 0 Then
			lsa_parametros = arg_parametros
			For ll_contador = UpperBound (lsa_parametros) To LowerBound (lsa_parametros) Step -1
				ls_parametro = "%" + Trim (String (ll_contador))
				is_mensaje = uof_sustituir_cadena (is_mensaje, ls_parametro, lsa_parametros [ll_contador])
			Next
		Else
			is_mensaje = uof_sustituir_cadena (is_mensaje, "%1", " ")
			is_mensaje = uof_sustituir_cadena (is_mensaje, "%2", " ")
			is_mensaje = uof_sustituir_cadena (is_mensaje, "%3", " ")
		End If	
	End If
End If

Return ll_fila
end function

public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono, any arg_buttom, long arg_defecto);// INTERFAZ
// Descripci�n:	Funci�n que muestra un MessageBox en pantalla con los argumentos
//						que se le pasan por par�metro y en el idioma correspondiente.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Any		arg_parama			=> Par�metros del t�tulo.
//		Long		arg_itemb			=> Texto del mensaje.
//		Any		arg_paramb			=>	Par�metros del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//		Any		arg_buttom			=> Botones del MessageBox.
//		Long		arg_defecto			=> N�mero del bot�n que se marcar� por defecto.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret
String ls_titulo, ls_texto

This.uof_init (arg_itema, arg_idioma, arg_parama)
ls_titulo = This.is_mensaje

This.uof_init (arg_itemb, arg_idioma, arg_paramb)
ls_texto = This.is_mensaje

/*[INI] R-NH-0032: Vinculaci�n con debugger - RWC - 2001/02/05*/
if gnvuo_debug.uof_init() = 1 then
	this.puof_LogCurrentProcess("uof_MessageBox(arg_itema,arg_parama,arg_itemb,arg_paramb,arg_idioma,arg_icono,arg_buttom,arg_defecto) - Title: " + string(arg_itema) + ", Message: " + string(arg_itemb))
	ls_titulo = ls_titulo + "(" + string(arg_itema) + ")"
	ls_texto = ls_texto + "(" + string(arg_itemb) + ")"
end if
/*[FIN] R-NH-0032: Vinculaci�n con debugger - RWC - 2001/02/05*/

ll_ret = MessageBox (ls_titulo, ls_texto, arg_icono, arg_buttom, arg_defecto)

Return ll_ret




end function

public function Long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, any arg_paramb, long arg_idioma);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//								icono, bot�n y por defecto
//						con lo cual el sistema pondr� los que tiene por defecto.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Any		arg_parama			=> Par�metros del t�tulo.
//		Long		arg_itemb			=> Texto del mensaje.
//		Any		arg_paramb			=>	Par�metros del mensaje.
//		Long		arg_idioma			=> Idioma.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________


Long  ll_ret

ll_ret = This.uof_messagebox (arg_itema, arg_parama, arg_itemb, arg_paramb, arg_idioma, Information!, OK!, 1)

Return ll_ret
end function

public function Long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//									bot�n y por defecto
//						con lo cual el sistema pondr� los que tiene por defecto.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Any		arg_parama			=> Par�metros del t�tulo.
//		Long		arg_itemb			=> Texto del mensaje.
//		Any		arg_paramb			=>	Par�metros del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, arg_parama, arg_itemb, arg_paramb, arg_idioma, arg_icono, OK!, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono, any arg_buttom);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//								el icono que se marcar� por defecto
//						con lo cual el sistema pondr� 1 por defecto.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Any		arg_parama			=> Par�metros del t�tulo.
//		Long		arg_itemb			=> Texto del mensaje.
//		Any		arg_paramb			=>	Par�metros del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//		Any		arg_buttom			=> Botones del MessageBox.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, arg_parama, arg_itemb, arg_paramb, arg_idioma, arg_icono, arg_buttom, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, long arg_idioma);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//							los par�metros del texto, icono, bot�n y por defecto 
//						con lo cual el sistema pondr� los que tiene por defecto.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Any		arg_parama			=> Par�metros del t�tulo.
//		Long		arg_itemb			=> Texto del mensaje.
//		Long		arg_idioma			=> Idioma.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, arg_parama, arg_itemb, '', arg_idioma, Information!, OK!, 1)

Return ll_ret

end function

public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, long arg_idioma, any arg_icono);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//							los par�metos del texto, bot�n y por defecto
//						con lo cual el sistema pondr� los que tiene por defecto.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Any		arg_parama			=> Par�metros del t�tulo.
//		Long		arg_itemb			=> Texto del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, arg_parama, arg_itemb, '', arg_idioma, arg_icono, OK!, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, long arg_idioma, any arg_icono, any arg_buttom);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//							los par�metros del texto, ni el icono por defecto
//									con lo cual el sistema pondr� 1.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Any		arg_parama			=> Par�metros del t�tulo.
//		Long		arg_itemb			=> Texto del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//		Any		arg_buttom			=> Botones del MessageBox.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, arg_parama, arg_itemb, '', arg_idioma, arg_icono, arg_buttom, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, any arg_parama, long arg_itemb, long arg_idioma, any arg_icono, any arg_buttom, long arg_defecto);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//								los par�metros del texto
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Any		arg_parama			=> Par�metros del t�tulo.
//		Long		arg_itemb			=> Texto del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//		Any		arg_buttom			=> Botones del MessageBox.
//		Long		arg_defecto			=> N�mero del bot�n que se marcar� por defecto.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, arg_parama, arg_itemb, '', arg_idioma, arg_icono, arg_buttom, arg_defecto)

Return ll_ret

end function

public function long uof_messagebox (long arg_itema, long arg_itemb, any arg_paramb, long arg_idioma);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//								los par�metros del t�tulo, icono, bot�n y por defecto
//						con lo cual el sistema pondr� los que tiene por defecto.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Long		arg_itemb			=> Texto del mensaje.
//		Any		arg_paramb			=>	Par�metros del mensaje.
//		Long		arg_idioma			=> Idioma.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, '', arg_itemb, arg_paramb, arg_idioma, Information!, OK!, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//								los par�metros del t�tulo, bot�n y por defecto
//						con lo cual el sistema pondr� los que tiene por defecto.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Long		arg_itemb			=> Texto del mensaje.
//		Any		arg_paramb			=>	Par�metros del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, '', arg_itemb, arg_paramb, arg_idioma, arg_icono, OK!, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono, any arg_buttom);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//								los par�metros del t�tulo ni el icono por defecto
//						con lo cual el sistema pondr� 1.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Long		arg_itemb			=> Texto del mensaje.
//		Any		arg_paramb			=>	Par�metros del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//		Any		arg_buttom			=> Botones del MessageBox.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, '', arg_itemb, arg_paramb, arg_idioma, arg_icono, arg_buttom, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, long arg_itemb, any arg_paramb, long arg_idioma, any arg_icono, any arg_buttom, long arg_defecto);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin especificar:
//								los par�metros del t�tulo.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Long		arg_itemb			=> Texto del mensaje.
//		Any		arg_paramb			=>	Par�metros del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//		Any		arg_buttom			=> Botones del MessageBox.
//		Long		arg_defecto			=> N�mero del bot�n que se marcar� por defecto.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, '', arg_itemb, arg_paramb, arg_idioma, arg_icono, arg_buttom, arg_defecto)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, long arg_itemb, long arg_idioma);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin par�metros en el 
//					   t�tulo ni en el texto y sin especificar:
//								icono, bot�n y por defecto
//						con lo cual el sistema pondr� los que tiene por defecto.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Long		arg_itemb			=> Texto del mensaje.
//		Long		arg_idioma			=> Idioma.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, '', arg_itemb, '', arg_idioma, Information!, OK!, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, long arg_itemb, long arg_idioma, any arg_icono);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin par�metros en el 
//					   t�tulo ni en el texto y sin especificar:
//								bot�n y bot�n por defecto
//						con lo cual el sistema pondr� los que tiene por defecto.
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Long		arg_itemb			=> Texto del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, '', arg_itemb, '', arg_idioma, arg_icono, OK!, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, long arg_itemb, long arg_idioma, any arg_icono, any arg_buttom);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin par�metros en el 
//					   t�tulo ni en el texto y sin especificar:
//								el bot�n por defecto
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Long		arg_itemb			=> Texto del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//		Any		arg_buttom			=> Botones del MessageBox.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, '', arg_itemb, '', arg_idioma, arg_icono, arg_buttom, 1)

Return ll_ret
end function

public function long uof_messagebox (long arg_itema, long arg_itemb, long arg_idioma, any arg_icono, any arg_buttom, long arg_defecto);// INTERFAZ
// Descripci�n:	Funci�n sobrecarga que muestra un mensaje sin par�metros en el 
//					   t�tulo ni en el texto y sin especificar:
//
// 		<uof_messagebox>
// 
// IN
//		Long		arg_itema			=> T�tulo del mensaje.
//		Long		arg_itemb			=> Texto del mensaje.
//		Long		arg_idioma			=> Idioma.
//		Any		arg_icono			=>	Icono del MessageBox.
//		Any		arg_buttom			=> Botones del MessageBox.
//		Long		arg_defecto			=> N�mero del bot�n que se marcar� por defecto.
//
// OUT
// 	Long 		ll_ret				=> Retorno del MessageBox.
//
// Creado	<16/11/1.999>		<M��ngeles Gracia>		<R-NH-2534 / HIS-274 / OT-876>
//__________________________________________________________________________________________

Long ll_ret

ll_ret = This.uof_messagebox (arg_itema, '', arg_itemb, '', arg_idioma, arg_icono, arg_buttom, arg_defecto)

Return ll_ret
end function

private function string uof_sustituir_cadena (string arg_cadena, string arg_buscada, string arg_sustituta);// INTERFAZ
// Descripci�n:	Cambia todas las subcadenas 'arg_buscada' por 'arg_sustituta' y 
//						devuelve la cadena resultante.
//						Sustituye a la funci�n global gf_sustituir_cadena.
//
//			<uof_sustituir_cadena>
//
// IN
//		String  arg_cadena			=> Cadena origen.
//		String  arg_buscada			=> Subcadena a buscar.
//		String  arg_sustituta		=> Subcadena a sustituir.
// 
// OUT
//		String 	ls_resultado			=> Resultado de la sustituci�n.
//
// Creado	<27/10/1.999>		<M��ngeles Gracia>			<R-NH-2534-274>
//__________________________________________________________________________________________

 
Long   ll_pos, ll_len_buscada, ll_len_sustituta
String ls_resultado

ls_resultado = arg_cadena

If (arg_buscada <> arg_sustituta) And (arg_buscada <> "") And (arg_sustituta <> "") Then
	ll_len_buscada   = Len (arg_buscada)
	ll_len_sustituta = Len (arg_sustituta)

	ll_pos = Pos (ls_resultado, arg_buscada)
	Do While ll_pos <> 0
		ls_resultado =	Replace (ls_resultado, ll_pos, ll_len_buscada, arg_sustituta)
		ll_pos	    =	Pos (ls_resultado, arg_buscada, ll_pos + ll_len_sustituta)
	Loop
End If

Return ls_resultado


end function

public function string uof_init (long al_msg_pk, any aa_args);/********************************************************************
	nvuo_items_mlg.uof_init(long,any) returns string

	<RETURN> string - Message en el idioma global con los argumentos</RETURN>

	<ACCESS> Public </ACCESS>

	<ARGS>	
				<LI>al_msg_pk: C�digo de Mensaje
				<LI>aa_args: Argumentos para el mensaje
	</ARGS>

	<USAGE>	ls_msg = lnvuo_itemsML.uof_Init(900012345, las_Args[])</USAGE>
	
	<DESC>
		Se llama al metodo uof_init(long,long,any) y si este devuelve 1, devuelve el mensaje
		generado.
	</TR>
	<TR><TD VALIGN=TOP WIDTH=200><B>Log de Cambios</B></TD>
	<TD>
	<TABLE  BORDER=1 WIDTH=100% BORDERCOLOR="#222222" CELLPADDING=3 CELLSPACING=0>

	<TR BGCOLOR="#DDDDDD">
	<TD>Fecha   </TD><TD>Ref</TD>                  <TD>Author</TD><TD>Comentario</TD></TR>
	<TR>
	<TD>2000/12/05</TD><TD>Fusi�n de Citas</TD><TD>RWC   </TD><TD>Creaci�n</TD></TR>

	</TABLE>
	</TD>
	</DESC>

********************************************************************/
long ll_Result
string ls_result

ll_Result = this.uof_init(al_msg_pk,gl_idioma,aa_args)

if ll_result > 0 then ls_result = this.is_mensaje

return ls_result
end function

public function string uof_init (long al_msg_pk);/********************************************************************
	nvuo_items_mlg.uof_init(long) returns string

	<RETURN> string - Message en el idioma global</RETURN>

	<ACCESS> Public </ACCESS>

	<ARGS>	
				<LI>al_msg_pk: C�digo de Mensaje
	</ARGS>

	<USAGE>	ls_msg = lnvuo_itemsML.uof_Init(900012345, las_Args[])</USAGE>
	
	<DESC>
		Se llama al metodo uof_init(long,long,any) y si este devuelve 1, devuelve el mensaje
		generado.
	</TR>
	<TR><TD VALIGN=TOP WIDTH=200><B>Log de Cambios</B></TD>
	<TD>
	<TABLE  BORDER=1 WIDTH=100% BORDERCOLOR="#222222" CELLPADDING=3 CELLSPACING=0>

	<TR BGCOLOR="#DDDDDD">
	<TD>Fecha   </TD><TD>Ref</TD>                  <TD>Author</TD><TD>Comentario</TD></TR>
	<TR>
	<TD>2000/12/05</TD><TD>Fusi�n de Citas</TD><TD>RWC   </TD><TD>Creaci�n</TD></TR>

	</TABLE>
	</TD>
	</DESC>

********************************************************************/
string las_temp[]

return this.uof_init(al_msg_pk,las_temp[])
end function

public function long uof_messagebox (long al_msg_id);/********************************************************************
	nvuo_items.mlg.uof_messagebox() returns long

	<RETURN> long - devuelve 1 si no hubo problemas</RETURN>

	<ACCESS> public </ACCESS>

	<ARGS>	
		Se muestra un mensaje sin especificar T�tulo, icono,botones o idioma
	</ARGS>

	<USAGE>	ls_result = lnvuo_item.uof_MessageBox(10000488) </USAGE>
	
	<DESC>

	</TR>
	<TR><TD VALIGN=TOP WIDTH=200><B>Log de Cambios</B></TD>
	<TD>
	<TABLE  BORDER=1 WIDTH=100% BORDERCOLOR="#222222" CELLPADDING=3 CELLSPACING=0>

	<TR BGCOLOR="#DDDDDD">
	<TD>Fecha   	</TD><TD>Ref</TD>          <TD>Author</TD><TD>Comentario</TD></TR>
	<TR>
	<TD>2001/02/01 </TD><TD>I-ISF-0015</TD><TD>RWC   </TD><TD>Creaci�n</TD></TR>

	</TABLE>
	</TD>
	</DESC>

********************************************************************/


any la_argsdummy[]

Return This.uof_messagebox (1, la_argsdummy[], al_msg_id, la_argsdummy[], gl_idioma)
end function

public function long uof_formatear (ref datawindow adw);long ll_Result
string ls_Result,ls_label
nvuo_string lnvuo_string
long intI
long ll_item
boolean lb_dospuntos

if isvalid(adw) then
	ls_Result = adw.describe("DataWindow.objects")
	if len(ls_Result) <> 0 then
		lnvuo_string = create nvuo_string
			lnvuo_string.uof_ParseString(ls_result,char(9))
			for intI = 1 to lnvuo_string.uof_GetItemCount()
				ls_label = lnvuo_String.uof_GetItem(intI)
				if right(ls_label,2) = "_t" then
					ls_result = adw.describe(ls_label + ".tag")
					lb_dospuntos = (right(adw.describe(ls_label + ".text"),1) = ":")
					if isnumber(ls_result) then
						if gnvuo_debug.uof_Init() <> 1 then
							ll_item = long(ls_result)
							ls_Result = this.uof_init(ll_item)
						end if
						if len(ls_Result) <> 0 then
							if lb_dospuntos then ls_result = ls_result + ":"
							adw.modify(ls_label + ".text='" + ls_result + "'")
						end if
					end if
				end if
			next
		destroy lnvuo_string
	end if
end if

return ll_Result
end function

public function long uof_formatear (ref userobject auo);string ls_result
long intI,ll_item,intY
windowobject lwo
string ls_tag
boolean lb_dospuntos
statictext lst
commandbutton lcmd
checkbox lcbx
groupbox lgb
picturebutton lpb
tab ltab
userobject luo

if isvalid(auo) then
	lwo = auo
	do while isvalid(lwo)
		Choose case lwo.typeof()
			case statictext!, commandbutton!, checkbox!,groupbox!,picturebutton!,userobject!
				ls_tag = lwo.tag
				if len(ls_tag) <> 0 and isnumber(ls_tag) then
					if gnvuo_Debug.uof_Init() = 1 then
						ls_Result = ls_tag
					else
						ll_item = long(ls_tag)
						ls_Result = this.uof_init(ll_item)
					end if
					if len(ls_Result) <> 0 then
						choose case lwo.typeof()
							case statictext!
								lst = lwo
								lb_dospuntos = (right(lst.text,1) = ":")
								if lb_dospuntos then ls_Result = ls_Result + ":"
								lst.text = ls_result
							case commandbutton!
								lcmd = lwo
								lb_dospuntos = (right(lcmd.text,1) = ":")
								if lb_dospuntos then ls_Result = ls_Result + ":"
								lcmd.text = ls_result
							case checkbox!
								lcbx = lwo
								lb_dospuntos = (right(lcbx.text,1) = ":")
								if lb_dospuntos then ls_Result = ls_Result + ":"
								lcbx.text = ls_result
							case groupbox!
								lgb = lwo
								lb_dospuntos = (right(lgb.text,1) = ":")
								if lb_dospuntos then ls_Result = ls_Result + ":"
								lgb.text = ls_result
							case picturebutton!
								lpb = lwo
								lb_dospuntos = (right(lpb.text,1) = ":")
								if lb_dospuntos then ls_Result = ls_Result + ":"
								lpb.text = ls_result
						case userobject!
								luo = lwo
								lb_dospuntos = (right(luo.text,1) = ":")
								if lb_dospuntos then ls_Result = ls_Result + ":"
								luo.text = ls_result
						end choose
					end if
				end if
			case tab!
				ltab = lwo
				for intY = 1 to upperbound(ltab.control[])
					luo = ltab.control[intY]
					ls_tag = luo.tag
					if len(ls_tag) <> 0 and isnumber(ls_tag) then
						if gnvuo_Debug.uof_Init() = 1 then
							ls_Result = ls_tag
						else
							ll_item = long(ls_tag)
							ls_Result = this.uof_init(ll_item)
							if ls_result <> "" then luo.text = ls_result
						end if
					end if
				next
			case else
		end choose
		intI ++
		if intI <= upperbound(auo.control[]) then
			lwo = auo.control[intI]
		else
			setnull(lwo)
		end if
	loop
end if
return 1
end function

public function long uof_formatear (datastore ads);long ll_Result
string ls_Result,ls_label
nvuo_string lnvuo_string
long intI
long ll_item
boolean lb_dospuntos

if isvalid(ads) then
	ls_Result = ads.describe("DataWindow.objects")
	if len(ls_Result) <> 0 then
		lnvuo_string = create nvuo_string
			lnvuo_string.uof_ParseString(ls_result,char(9))
			for intI = 1 to lnvuo_string.uof_GetItemCount()
				ls_label = lnvuo_String.uof_GetItem(intI)
				if right(ls_label,2) = "_t" then
					ls_result = ads.describe(ls_label + ".tag")
					lb_dospuntos = (right(ads.describe(ls_label + ".text"),1) = ":")
					if isnumber(ls_result) then
						if gnvuo_debug.uof_Init() <> 1 then
							ll_item = long(ls_result)
							ls_Result = this.uof_init(ll_item)
						end if
						if len(ls_Result) <> 0 then
							if lb_dospuntos then ls_result = ls_result + ":"
							ads.modify(ls_label + ".text='" + ls_result + "'")
						end if
					end if
				end if
			next
		destroy lnvuo_string
	end if
end if

return ll_Result
end function

public function long uof_formatear (datawindowchild adwc);long ll_Result
string ls_Result,ls_label
nvuo_string lnvuo_string
long intI
long ll_item
boolean lb_dospuntos

if isvalid(adwc) then
	ls_Result = adwc.describe("DataWindow.objects")
	if len(ls_Result) <> 0 then
		lnvuo_string = create nvuo_string
			lnvuo_string.uof_ParseString(ls_result,char(9))
			for intI = 1 to lnvuo_string.uof_GetItemCount()
				ls_label = lnvuo_String.uof_GetItem(intI)
				if right(ls_label,2) = "_t" then
					ls_result = adwc.describe(ls_label + ".tag")
					lb_dospuntos = (right(adwc.describe(ls_label + ".text"),1) = ":")
					if isnumber(ls_result) then
						if gnvuo_debug.uof_Init() <> 1 then
							ll_item = long(ls_result)
							ls_Result = this.uof_init(ll_item)
						end if
						if len(ls_Result) <> 0 then
							if lb_dospuntos then ls_result = ls_result + ":"
							adwc.modify(ls_label + ".text='" + ls_result + "'")
						end if
					end if
				end if
			next
		destroy lnvuo_string
	end if
end if

return ll_Result
end function

on nvuo_items_mlg.create
TriggerEvent( this, "constructor" )
end on

on nvuo_items_mlg.destroy
TriggerEvent( this, "destructor" )
end on

event constructor;call super::constructor;// INTERFAZ
// Descripci�n:	Se usa la ds de instancia del padre para almacenar la dw.
//
//		<constructor>
//
// Creado	<26/10/1.999>	<M��ngeles Gracia>		<R-NH-2534/274>
//__________________________________________________________________________________________


ids_nvuo.DataObject = 'dw_nvuo_items_mlg'
ids_nvuo.SetTransObject (SQLCA)
end event

